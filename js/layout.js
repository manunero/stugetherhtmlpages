/*
Developer : Ponraj Paul
*/
(function($, window, document) {
$(function() {
	'use strict';
	var winWidth, winHeight, isNav = false, isSupported = {
		svg: function() {
  			return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1");
		},
		touch : function(){
			return 'ontouchstart' in window || !!(navigator.msMaxTouchPoints);
		},
		screenSize : function(){
			var e = window, a = 'inner';
			if (!('innerWidth' in window )) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ]};
		},			
		canvas : function(){
			return !!document.createElement('canvas').getContext;
		},		
		localStorage : function(){
			try {
				return 'localStorage' in window && window['localStorage'] !== null;
			} catch(e){
				return false;
			}
		}
	};
	
	$('.navIcon').on('click', function(e){
		var $this = $(this).next();
		if($this.hasClass('active')){
			$this.removeClass('active');
		}else{
			$this.addClass('active');
		}
		e.preventDefault();
	});
	$('.close').on('click',function(e){
		$('.overlay').removeClass('Opened');
		$('.overlay').fadeOut(1000);
		$('.addPeoples').slideUp(300);
		e.preventDefault();
	});
	$('.addFollower').on('click', function(e){
		$('.addPeoples').slideDown(300);
		e.preventDefault();
	});
	$('#showMenu').on('click',function(e){
		$('.overlay').fadeIn(500,function(){
			$('.overlay').addClass('Opened');
		});
		e.preventDefault();
	});
	$('.discussion_cover .grid_box a').on('click',function(e){
		$('.overlay').fadeIn(500,function(){
			$('.overlay').addClass('Opened');
		});
		e.preventDefault();
	});
	$(".head_cover .tab1").on('click',function(e){
		$(".group_cover").hide();
		$(".head_cover .tab1").addClass("activate");
		$(".head_cover .tab2").removeClass("activate");
		$(".discussion_cover").show();
		e.preventDefault();
	});
	$(".head_cover .tab2").on('click',function(e){
		$(".group_cover").show();
		$(".head_cover .tab1").removeClass("activate");
		$(".head_cover .tab2").addClass("activate");
		$(".discussion_cover").hide();
		e.preventDefault();
	});
	$('.myPop').on('click', '.edit', function(e){
		$(this).closest('.column').addClass('editable');
		e.preventDefault();
	});
	$('.myPop').on('click', '.submit', function(e){
		//your function to submit the form data will come here
		$(this).closest('.column').removeClass('editable');
		e.preventDefault();
	});
	$('.notification').on('click', function(e){
		var $this = $(this);
		if($this.hasClass('active')){
			$this.removeClass('active');
			$('.notifications').slideUp(300);
		}else{
			$this.addClass('active');
			$('.notifications').slideDown(300);
		}
		e.preventDefault();
	});
	$('.newFeedIcon').on('click', function(e){
		$('.feedWidget, .chooseFeedType').addClass('active');
		e.preventDefault();
	});
	$('.chooseFeedType').on('click', 'a', function(e){
		$('.chooseFeedType').removeClass('active');
		var thisId = $(this).attr('href');
		thisId = thisId.split('#')[1];
		$('.form, .form .'+thisId).addClass('active');
		e.preventDefault();
	});
	$('.formBack').on('click', function(e){
		$('.form, .formKid').removeClass('active');
		$('.chooseFeedType').addClass('active');
		e.preventDefault();
	});
	$('.formClose').on('click', function(e){
		$('.formKid, .form, .chooseFeedType, .feedWidget').removeClass('active');
		e.preventDefault();
	});
	setInterval(function(){ appendFeed() }, 3000);
	(function(){
   	 	$('.groupBanner').css('height', isSupported.screenSize().height - 260 );
		$('.groupDetailPage').removeAttr('style');
	})();
	
	$('#grid').on('click', '.openFeed', function(e){
		$('.slideView').addClass('active');
		$('body').css('overflow-y','scroll');
		$('html').css('overflow-y','hidden');
		e.preventDefault();
	});
	$('.slideClose').on('click', function(e){
		$('.slideView').removeClass('active');
		$('body').removeAttr('style');
		$('html').removeAttr('style');
		e.preventDefault();
	});
	$('.newFeedIcon').hover(function(){
			$('.chooseFeedType').addClass('active');
		}, function(){}
	);
	$('.uploadWidget').hover(function(){}, function(){
		$('.chooseFeedType').removeClass('active');
	});
	$('#grid').masonry({
		itemSelector: '.gridLi'
	});
	$('.chooseFeedType').on('click', 'a', function(e){
		$('.uploader').addClass('active');
		e.preventDefault();
	});
});
var i = 1;
function appendFeed(){
	i = i+1;
	if(i < 20){
		var img = '';
		if(i % 4 === 0 ){
			img = '1';
		}else if(i % 3 === 0 ){
			img = '2';
		}else if(i % 2 === 0 ){
			img = '3';
		}else{
			img = '2';
		}
		var data = '<li class="gridLi"><div class="feedWrap"><div class="feedProfile clear"><div class="profilePic"><img src="images/discussions/1.png" alt=""></div><div class="headDetails"><div class="userName"><a href="#">Don Walter</a></div><div class="postTime">23 mins ago</div></div></div><div class="feedImage"><a href="#" class="openFeed"><img src="images/feeds/'+img+'.jpg" alt=""></a></div><div class="feedData"><h2><a href="#" class="openFeed">Nunc convallis lacinia odio, ac ornare metus. Duis et telluIn mi ligula, porta non risus vel, euismod.</a></h2><p>Nunc convallis lacinia odio, ac ornare metus. Duis et telluIn mi ligul porta non risus vel, euismod. Nunc convallis lacinia odio, ac ornare metus. Duis et telluIn mi ligula, porta non risus vel, euismod. Nunc convallis lacinia odio, ac ornare metus. Duis et telluIn mi ligula, porta non risus vel, euismod. Nunc convallis lacinia odio, ac ornare metus. Duis et telluIn mi ligula, porta non risus vel, euismod. </p></div><div class="actions clear"><div class="source">source: <a href="http://cnn.com/" target="_blank">www.cnn.com</a></div><div class="options"><ul class="clear"><li><a href="#star" class="starIcon">26</a></li><li><a href="#comments" class="commentIcon">38</a></li><li><a href="#create" class="createNewIcon"></a></li><li><a href="#more" class="viewMoreIcon"></a></li></ul></div></div></div></li>';
		$('#grid').append(data);
		$('#grid').masonry('appended',data);
		$('#grid').masonry( 'reloadItems' );
		$('#grid').masonry( 'layout' );
		//$('#grid').masonry();
	}
}
}(window.jQuery, window, document));